Documentation: raspberry pi rekognition
***************************************

.. toctree::
   :maxdepth: 2

   rekognition
   djangoserver


La documentation suivante explique comment démarrer le
serveur Django, comment l'utiliser, comment celui-ci marche et comment utiliser
le package Rekognition.


  Rekognition package
  ====================================================
  Texte pour le package de Rekognition

  Serveur Django
  ====================================================
  Texte pour le serveur Django
